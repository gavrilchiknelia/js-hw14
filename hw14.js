(function($) {
    $(function() {

        $('ul.tabs-header').on('click', 'li:not(.active)', function() {
            $(this)
                .addClass("active")
                .siblings()
                .removeClass("active")
                .closest("div.tabs")
                .find("li.tab-b")
                .removeClass("show")
                .eq($(this).index())
                .addClass("show");
        });

    });
})(jQuery);


